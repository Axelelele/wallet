package se.axel.wallet.transaction.repository;

import org.springframework.stereotype.Repository;

import se.axel.wallet.database.AbstractDatabaseRepository;
import se.axel.wallet.transaction.domain.WalletTransaction;

/**
 * WalletTransaction subclass of AbstractDatabaseRepository, handling the persistence layer calls.
 * Currently only used by the transaction ID validator service.
 * @author Axel Jonsson
 */
@Repository
public class WalletTransactionRepository extends AbstractDatabaseRepository {

	/**
	 * Retrieves a WalletTransaction specified by transactionId
	 * @param transactionId The transaction to retrieve
	 * @return The transaction specified by transactionId
	 */
	public WalletTransaction getById(String transactionId) {
		return em.find(WalletTransaction.class, transactionId);
	}

}
