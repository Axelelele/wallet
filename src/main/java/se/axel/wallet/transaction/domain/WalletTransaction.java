package se.axel.wallet.transaction.domain;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonIgnore;

import se.axel.wallet.domain.Wallet;

@Entity
public class WalletTransaction {

	@Id
	private String transactionId;
	
	@Enumerated(EnumType.STRING)
	private WalletTransactionType transactionType;

	@Basic
	private Date transactionTime;
	
	@Min(value = 0)
	private long amount;
	
	public String getTransactionId() {
		return transactionId;
	}

	public WalletTransactionType getTransactionType() {
		return transactionType;
	}

	public long getAmount() {
		return amount;
	}
	
	public Date getTransactionTime() {
		return transactionTime;
	}
	
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}
	
	public static class Builder {
		private String transactionId;
		private WalletTransactionType transactionType;
		private Date transactionTime;
		private long amount;

		public Builder transactionId(String transactionId) {
			this.transactionId = transactionId;
			return this;
		}

		public Builder transactionType(WalletTransactionType transactionType) {
			this.transactionType = transactionType;
			return this;
		}
		
		public Builder transactionTime(Date transactionTime) {
			this.transactionTime = transactionTime;
			return this;
		}

		public Builder amount(long amount) {
			this.amount = amount;
			return this;
		}

		public WalletTransaction build() {
			WalletTransaction walletTransaction = new WalletTransaction();
			walletTransaction.transactionId = this.transactionId;
			walletTransaction.transactionType = this.transactionType;
			walletTransaction.transactionTime = this.transactionTime;
			walletTransaction.amount = this.amount;
			return walletTransaction;
		}
	}
}
