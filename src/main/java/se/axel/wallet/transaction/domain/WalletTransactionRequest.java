package se.axel.wallet.transaction.domain;

public class WalletTransactionRequest {

	private WalletTransactionType transactionType;
	private long amount;


	public WalletTransactionType getTransactionType() {
		return transactionType;
	}

	public long getAmount() {
		return amount;
	}
	
	public static class Builder {
		private WalletTransactionType transactionType;
		private long amount;

		public Builder transactionType(WalletTransactionType transactionType) {
			this.transactionType = transactionType;
			return this;
		}
		
		public Builder amount(long amount) {
			this.amount = amount;
			return this;
		}

		public WalletTransactionRequest build() {
			WalletTransactionRequest walletTransactionRequest = new WalletTransactionRequest();
			walletTransactionRequest.transactionType = this.transactionType;
			walletTransactionRequest.amount = this.amount;
			return walletTransactionRequest;
		}
	}
}
