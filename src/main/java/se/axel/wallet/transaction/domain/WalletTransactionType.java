package se.axel.wallet.transaction.domain;

public enum WalletTransactionType {
	DEBIT, CREDIT
}
