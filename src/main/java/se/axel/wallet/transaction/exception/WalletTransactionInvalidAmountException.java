package se.axel.wallet.transaction.exception;

import se.axel.wallet.exception.WalletException;

public class WalletTransactionInvalidAmountException extends WalletException {

	private static final long serialVersionUID = 1L;

	public WalletTransactionInvalidAmountException(String message) {
		super(message);
	}
	
}
