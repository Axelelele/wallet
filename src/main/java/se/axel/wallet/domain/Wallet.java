package se.axel.wallet.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import se.axel.wallet.transaction.domain.WalletTransaction;

/**
 * Wallet domain class. Holds the transactions and balance among other fields.
 * @author Axel Jonsson
 */
@JsonInclude(Include.NON_NULL)
@Entity
public class Wallet {
	
	/**
	 * Used for pessimistic database locking.
	 */
	@JsonIgnore
	@Version
	private long version;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer walletId;
	
	@Basic
	private long balance;
	
	@JoinColumn(name = "walletId")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<WalletTransaction> transactions;
	
	@Basic
	private Date lastTransactionTime;
	
	public Integer getWalletId() {
		return this.walletId;
	}
	
	public long getBalance() {
		return this.balance;
	}
	
	public List<WalletTransaction> getTransactions() {
		return this.transactions;
	}
	
	public Date getLastTransactionTime() {
		return this.lastTransactionTime;
	}
	
	public void setLastTransactionTime(Date lastTransactionTime) {
		this.lastTransactionTime = lastTransactionTime;
	}
	
	public void setBalance(long balance) {
		this.balance = balance;
	}
	
	public void addTransaction(WalletTransaction transaction) {
		if (transactions == null) {
			transactions = new ArrayList<>();
		}
		transactions.add(transaction);
	}
	
	public static class Builder {
		private Integer walletId;
		private long balance;
		private List<WalletTransaction> transactions;
		private Date lastTransactionTime;

		public Builder walletId(Integer customerId) {
			this.walletId = customerId;
			return this;
		}
		
		public Builder balance(long balance) {
			this.balance = balance;
			return this;
		}
		
		public Builder transactions(List<WalletTransaction> transactions) {
			this.transactions = transactions;
			return this;
		}
		
		public Builder lastTransaction(Date lastTransactionTime) {
			this.lastTransactionTime = lastTransactionTime;
			return this;
		}
		
		public Wallet build() {
			Wallet wallet = new Wallet();
			wallet.walletId = this.walletId;
			wallet.balance = this.balance;
			wallet.transactions = this.transactions;
			wallet.lastTransactionTime = this.lastTransactionTime;
			return wallet;
		}
	}
}
