package se.axel.wallet.service;

import java.util.Date;

import javax.persistence.LockModeType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.axel.wallet.domain.Wallet;
import se.axel.wallet.exception.WalletException;
import se.axel.wallet.exception.WalletInsufficientFundsException;
import se.axel.wallet.exception.WalletNotFoundException;
import se.axel.wallet.repository.WalletRepository;
import se.axel.wallet.transaction.domain.WalletTransaction;
import se.axel.wallet.transaction.domain.WalletTransactionRequest;
import se.axel.wallet.transaction.exception.WalletTransactionInvalidAmountException;

/**
 * The WalletService class contains the majority of the business logic for the
 * application, as well as a connection to the repository layer.
 * 
 * @author Axel Jonsson
 */
@Service
public class WalletService {

	private WalletRepository walletRepository;

	@Autowired
	public WalletService(WalletRepository walletRepository) {
		this.walletRepository = walletRepository;
	}

	/**
	 * Assembles a new instance of wallet and persists it to the database.
	 * @return The persisted wallet
	 */
	public Wallet createWallet() {
		Wallet wallet = new Wallet.Builder().balance(0).build();
		return walletRepository.insert(wallet);
	}

	/**
	 * Used in the context of registering transactions. Retrieves a wallet
	 * specified by wallet ID and applies a pessimistic lock. I chose safety
	 * over concurrency in this case, due to the service being light weight and
	 * the sensitive nature of currency transactions.
	 * 
	 * @param walletId The wallet to retrieve
	 * @return The wallet specified by walletId
	 */
	public Wallet getWalletByIdForUpdate(Integer walletId) {
		return getById(walletId, LockModeType.PESSIMISTIC_FORCE_INCREMENT);
	}

	/**
	 * Used in the context of read-only. Retrieves a wallet without applying a lock.
	 * @param walletId The wallet to retrieve
	 * @return The wallet specified by walletId
	 */
	public Wallet getWalletById(Integer walletId) {
		return getById(walletId, LockModeType.NONE);
	}

	/**
	 * Retrieves a wallet specified by walletId, and locks the entity as supplied.
	 * @param walletId The wallet to retrieve
	 * @param lockMode The lock mode to apply
	 * @return The wallet specified by walletId
	 * @throws WalletNotFoundException if the specified walletId could not be found in the database
	 */
	private Wallet getById(Integer walletId, LockModeType lockMode) {
		Wallet wallet = walletRepository.getById(walletId, lockMode);
		if (wallet == null) {
			throw new WalletNotFoundException(String.format("Wallet id %d not found.", walletId));
		}
		return wallet;
	}

	/**
	 * Registers a transaction to a wallet, after performing validations.
	 * 
	 * @param transactionId The transaction ID to attach to this transaction
	 * @param walletId The wallet on which to register the transaction
	 * @param transactionRequest The transaction request, containing amount and transaction type.
	 * @return The updated wallet with the processed transaction attached.
	 * @throws WalletTransactionInvalidAmountException If the requests specifies an invalid transaction amount
	 * @throws
	 */
	public Wallet registerTransaction(String transactionId, Integer walletId,
			WalletTransactionRequest transactionRequest) {

		//Validate
		validateRequest(transactionRequest);
		
		//Retrieve wallet, applying a pessimistic lock
		Wallet wallet = getWalletByIdForUpdate(walletId);
		
		//Convert the request to an internal transaction
		WalletTransaction transaction = toInternalTransactionModel(transactionId,
				transactionRequest);

		//Apply the changes to the wallet
		wallet.setLastTransactionTime(transaction.getTransactionTime());
		wallet.addTransaction(transaction);

		switch (transactionRequest.getTransactionType()) {
		case CREDIT:
			handleCreditTransaction(wallet, transaction);
			break;
		case DEBIT:
			handleDebitTransaction(wallet, transaction);
			break;
		default:
			throw new WalletException("Internal server error when evaluating transaction type.");
		}

		//If all went well, update the persistence context immediately before returning the updated wallet
		return walletRepository.update(wallet);
	}

	/**
	 * Validates the transaction request. As of now, our only requirement is
	 * that the transaction amount is a positive number.
	 * @param transactionRequest The request to validate
	 * @throws WalletTransactionInvalidAmountException if the transaction amount is <= 0
	 */
	private void validateRequest(WalletTransactionRequest transactionRequest) {
		if (transactionRequest.getAmount() <= 0) {
			throw new WalletTransactionInvalidAmountException(
					"Transaction amount must be greater than or equal to 0.");
		}
	}

	/**
	 * Adds the transaction amount to the wallets current balance.
	 * @param wallet The wallet on which to add
	 * @param transaction The transaction specifying the amount
	 */
	private void handleCreditTransaction(Wallet wallet, WalletTransaction transaction) {
		wallet.setBalance(wallet.getBalance() + transaction.getAmount());
	}

	/**
	 * Subtracts the transaction amount from the wallets current balance.
	 * @param wallet The wallet from which to subtract
	 * @param transaction The transaction specifying the amount
	 * @throws WalletInsufficientFundsException If the supplied wallets funds are insufficient to process the transaction
	 */
	private void handleDebitTransaction(Wallet wallet, WalletTransaction transaction) {
		long remainingBalance = wallet.getBalance() - transaction.getAmount();
		if (remainingBalance >= 0) {
			wallet.setBalance(remainingBalance);
		} else {
			throw new WalletInsufficientFundsException(
					"Transaction failed due to insufficient funds.");
		}
	}

	/**
	 * Assembles the external request model to an internal one, applying the
	 * supplied transaction ID and setting a transaction time
	 * 
	 * @param transactionId The transaction ID to apply
	 * @param transactionRequest The external request
	 * @return a WalletTransaction corresponding to the request
	 */
	private WalletTransaction toInternalTransactionModel(String transactionId,
			WalletTransactionRequest transactionRequest) {
		return new WalletTransaction.Builder()
					.amount(transactionRequest.getAmount())
					.transactionId(transactionId)
					.transactionType(transactionRequest.getTransactionType())
					.transactionTime(new Date()).build();
	}
}
