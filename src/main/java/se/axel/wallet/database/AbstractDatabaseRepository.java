package se.axel.wallet.database;

import javax.persistence.EntityManager;
//import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * Base class for database repositories, declaring common behaviour and fields.
 * @author Axel Jonsson
 */
@Transactional(rollbackOn = { Exception.class })
public abstract class AbstractDatabaseRepository {
	
	@PersistenceContext
	protected EntityManager em;
	
}
