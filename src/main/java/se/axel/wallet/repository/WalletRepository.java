package se.axel.wallet.repository;

import javax.persistence.LockModeType;

import org.springframework.stereotype.Repository;

import se.axel.wallet.database.AbstractDatabaseRepository;
import se.axel.wallet.domain.Wallet;

/**
 * Wallet subclass of AbstractDatabaseRepository, handling the persistence layer calls.
 * @author Axel Jonsson
 */
@Repository
public class WalletRepository extends AbstractDatabaseRepository {

	/**
	 * Persists a wallet to the database, flushing immediately to access the generated ID
	 * @param wallet The wallet to persist
	 * @return The persisted wallet, now populated with an ID.
	 */
	public Wallet insert(Wallet wallet) {
		em.persist(wallet);
		em.flush();
		return wallet;
	}
	
	/**
	 * Retrieves a wallet specified by walletId, and locks the entity according to lockMode.
	 * @param walletId The wallet to retrieve
	 * @param lockMode The lock to apply
	 * @return The wallet specified by walletId
	 */
	public Wallet getById(Integer walletId, LockModeType lockMode) {
		return em.find(Wallet.class, walletId, lockMode);
	}
	
	/**
	 * Merges the entity into the persistence context. Used for applying registered transactions and other values.
	 * @param wallet The wallet to update
	 * @return The updated wallet
	 */
	public Wallet update(Wallet wallet) {
		return em.merge(wallet);
	}

}
