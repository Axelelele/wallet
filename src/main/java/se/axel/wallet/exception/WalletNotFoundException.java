package se.axel.wallet.exception;

public class WalletNotFoundException extends WalletException {

	private static final long serialVersionUID = 1L;

	public WalletNotFoundException(String message) {
		super(message);
	}
	
}
