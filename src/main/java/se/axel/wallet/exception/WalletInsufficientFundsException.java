package se.axel.wallet.exception;

public class WalletInsufficientFundsException extends WalletException {

	private static final long serialVersionUID = 1L;

	public WalletInsufficientFundsException(String message) {
		super(message);
	}
	
}
