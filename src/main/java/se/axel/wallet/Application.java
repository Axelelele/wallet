package se.axel.wallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Application main class.
 * @author Axel
 */
@SpringBootApplication
@ComponentScan(basePackages="se.axel.wallet")
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
