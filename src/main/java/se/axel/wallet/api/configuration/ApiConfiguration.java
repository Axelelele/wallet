package se.axel.wallet.api.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import se.axel.wallet.api.common.transactionid.TransactionIdValidationInterceptor;

/**
 * API configuration class for registering the transaction ID interceptor.
 * @author Axel Jonsson
 */
@Configuration
public class ApiConfiguration extends WebMvcConfigurerAdapter {

	/**
	 * "Bean instantiator", so that Spring can autowire properly.
	 */
	@Bean
	public TransactionIdValidationInterceptor transactionIdValidationInterceptor() {
		return new TransactionIdValidationInterceptor();
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(transactionIdValidationInterceptor());
	}
}
