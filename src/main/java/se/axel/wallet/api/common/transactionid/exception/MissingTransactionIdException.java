package se.axel.wallet.api.common.transactionid.exception;

import se.axel.wallet.exception.WalletException;

public class MissingTransactionIdException extends WalletException {

	private static final long serialVersionUID = 1L;

	public MissingTransactionIdException(String message) {
		super(message);
	}

}
