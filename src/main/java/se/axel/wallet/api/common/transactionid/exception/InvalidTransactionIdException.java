package se.axel.wallet.api.common.transactionid.exception;

import se.axel.wallet.exception.WalletException;

public class InvalidTransactionIdException extends WalletException {

	private static final long serialVersionUID = 1L;

	public InvalidTransactionIdException(String message) {
		super(message);
	}

}
