package se.axel.wallet.api.common.transactionid;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Implementing methods shall be validated to contain a proper transaction ID.
 * @author Axel Jonsson
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface TransactionIdRequired {

}
