package se.axel.wallet.api.common.transactionid.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.axel.wallet.api.common.transactionid.exception.DuplicateTransactionIdException;
import se.axel.wallet.api.common.transactionid.exception.InvalidTransactionIdException;
import se.axel.wallet.api.common.transactionid.exception.MissingTransactionIdException;
import se.axel.wallet.transaction.repository.WalletTransactionRepository;

/**
 * The transaction ID validator service serves to assure that a supplied transaction ID is of valid type UUID.
 * @author Axel Jonsson
 */
@Service
public class TransactionIdValidatorService {

	private WalletTransactionRepository transactionRepository;

	@Autowired
	public TransactionIdValidatorService(WalletTransactionRepository transactionRepository) {
		this.transactionRepository = transactionRepository;
	}

	/**
	 * Validates a given transaction ID.
	 * @param transactionId The transaction ID to validate
	 * @return true, if supplied transaction ID is a valid UUID.
	 * @throws MissingTransactionIdException If no transaction ID is supplied
	 * @throws InvalidTransactionIdException If the supplied transaction ID is not a valid UUID
	 * @throws DuplicateTransactionIdException If the supplied transaction ID already has been processed
	 */
	public boolean validateTransactionId(String transactionId) {
		if (transactionId == null) {
			throw new MissingTransactionIdException("Missing request header 'Transaction-ID'.");
		}
		try {
			UUID.fromString(transactionId);
		} 
		catch (Exception e) {
			throw new InvalidTransactionIdException(String.format("Invalid transaction id: '%s'. Must be of type UUID.", transactionId));
		}
		if (transactionRepository.getById(transactionId) != null) {
			throw new DuplicateTransactionIdException(String.format("Duplicate transaction id '%s'.", transactionId));
		}
		return true;
	}
}
