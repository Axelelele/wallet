package se.axel.wallet.api.common.transactionid;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import se.axel.wallet.api.common.ApiHeaderKey;
import se.axel.wallet.api.common.transactionid.service.TransactionIdValidatorService;

/**
 * Interceptor class for transaction ID validation. Intercepts incoming requests, and queries {@link TransactionIdValidatorService} for validation.
 * @author Axel Jonsson
 */
public class TransactionIdValidationInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	TransactionIdValidatorService transactionIdValidator;

	@Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, 
                             Object handler) throws Exception {
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		
		if (handlerMethod.getMethod().isAnnotationPresent(TransactionIdRequired.class))
		{
			return transactionIdValidator.validateTransactionId(request.getHeader(ApiHeaderKey.TRANSACTION_ID));
		}
		
		return true;
	}
}
