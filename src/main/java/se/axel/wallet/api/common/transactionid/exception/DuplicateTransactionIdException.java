package se.axel.wallet.api.common.transactionid.exception;

import se.axel.wallet.exception.WalletException;

public class DuplicateTransactionIdException extends WalletException {

	private static final long serialVersionUID = 1L;

	public DuplicateTransactionIdException(String message) {
		super(message);
	}

}
