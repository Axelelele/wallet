package se.axel.wallet.api.common;

/**
 * Contains common API Header keys in the context of the application. Just one, for now...
 * @author Axel Jonsson
 */
public class ApiHeaderKey {
	public static String TRANSACTION_ID = "Transaction-ID";
}
