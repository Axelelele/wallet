package se.axel.wallet.api.exception.mapper;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import se.axel.wallet.api.common.transactionid.exception.DuplicateTransactionIdException;
import se.axel.wallet.api.common.transactionid.exception.InvalidTransactionIdException;
import se.axel.wallet.api.common.transactionid.exception.MissingTransactionIdException;
import se.axel.wallet.exception.WalletInsufficientFundsException;
import se.axel.wallet.exception.WalletNotFoundException;
import se.axel.wallet.transaction.exception.WalletTransactionInvalidAmountException;

/**
 * Response exception mapper for translating application errors into HTTP responses.
 * @author Axel Jonsson
 */
@ControllerAdvice
public class ResponseExceptionMapper extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { Throwable.class })
	protected ResponseEntity<String> mapException(Throwable exception, WebRequest request) {
		
		if (exception instanceof WalletNotFoundException) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.TEXT_PLAIN).body(exception.getMessage());
		}
		if (exception instanceof DuplicateTransactionIdException) {
			return ResponseEntity.status(HttpStatus.CONFLICT).contentType(MediaType.TEXT_PLAIN).body(exception.getMessage());
		}
		if (exception instanceof InvalidTransactionIdException) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.TEXT_PLAIN).body(exception.getMessage());
		}
		if (exception instanceof MissingTransactionIdException) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.TEXT_PLAIN).body(exception.getMessage());
		}
		if (exception instanceof WalletInsufficientFundsException) {
			return ResponseEntity.status(HttpStatus.CONFLICT).contentType(MediaType.TEXT_PLAIN).body(exception.getMessage());
		}
		if (exception instanceof WalletTransactionInvalidAmountException) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.TEXT_PLAIN).body(exception.getMessage());
		}
		
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.TEXT_PLAIN).body("Unhandled exception");
	}
}
