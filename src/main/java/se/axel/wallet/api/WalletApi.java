package se.axel.wallet.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import se.axel.wallet.api.common.transactionid.TransactionIdRequired;
import se.axel.wallet.domain.Wallet;
import se.axel.wallet.service.WalletService;
import se.axel.wallet.transaction.domain.WalletTransactionRequest;

/**
 * Wallet Service API implementation.
 * @author Axel Jonsson
 */
@RestController
public class WalletApi {

	private WalletService walletService;
	
	@Autowired
	public WalletApi(WalletService walletService) {
		this.walletService = walletService;
	}

	/**
	 * Creates a new wallet and assigns it a wallet ID.
	 * @return The created wallet.
	 */
	@PostMapping(value = "/wallet")
	ResponseEntity<Wallet> createWallet() {
		Wallet wallet = walletService.createWallet();
		return ResponseEntity.ok().body(wallet);
	}
	
	/**
	 * Returns a wallet specified by path variable walletId, including any previous transactions.
	 * @param walletId The wallet to retrieve
	 * @return The specified wallet
	 */
	@GetMapping(value = "/wallet/{walletId}", produces = "application/json")
	ResponseEntity<Wallet> getWalletById(@PathVariable Integer walletId) {
		Wallet wallet = walletService.getWalletById(walletId);
		return ResponseEntity.ok().body(wallet);
	}

	/**
	 * Registers a transaction to an existing wallet.
	 * @param transactionId Unique transaction ID of type UUID (RFC 4122)
	 * @param walletId The wallet which to register the transaction to.
	 * @param transactionRequest The request model of a transaction, containing only the necessary input data
	 * @return The updated wallet along with any processed transactions.
	 */
	@TransactionIdRequired
	@PostMapping(value = "/wallet/{walletId}", produces = "application/json")
	ResponseEntity<Wallet> registerTransaction(@RequestHeader(value = "Transaction-ID") String transactionId, 
											   @PathVariable Integer walletId, 
											   @RequestBody WalletTransactionRequest transactionRequest) {
		Wallet wallet = walletService.registerTransaction(transactionId, walletId, transactionRequest);
		return new ResponseEntity<Wallet>(wallet, HttpStatus.OK);
	}
}

