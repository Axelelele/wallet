package se.axel.wallet.api;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import se.axel.wallet.domain.Wallet;
import se.axel.wallet.service.WalletService;
import se.axel.wallet.test.TestData;
import se.axel.wallet.transaction.domain.WalletTransaction;
import se.axel.wallet.transaction.domain.WalletTransactionRequest;

@RunWith(MockitoJUnitRunner.class)
public class WalletApiTest {

	@Mock
	private WalletService walletServiceMock;

	private WalletApi walletApi;


	@Before
	public void setup() {
		walletApi = new WalletApi(walletServiceMock);
	}

	@Test
	public void test_createWallet() {
		// Setup
		when(walletServiceMock.createWallet()).thenReturn(TestData.NEW_WALLET_WITH_ID);

		// Execute
		ResponseEntity<Wallet> response = walletApi.createWallet();

		// Verify
		Assert.assertEquals(TestData.NEW_WALLET_WITH_ID, response.getBody());
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		verify(walletServiceMock).createWallet();
	}
	
	@Test
	public void test_getWalletById() {
		// Setup
		when(walletServiceMock.getWalletById(any(Integer.class))).thenReturn(TestData.NEW_WALLET_WITH_ID);
		Integer walletId = 1;

		// Execute
		ResponseEntity<Wallet> response = walletApi.getWalletById(walletId);

		// Verify
		Assert.assertEquals(TestData.NEW_WALLET_WITH_ID, response.getBody());
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		verify(walletServiceMock).getWalletById(walletId);
	}
	
	@Test
	public void test_registerTransaction() {
		// Setup
		when(walletServiceMock.registerTransaction(anyString(), any(Integer.class), any(WalletTransactionRequest.class))).thenReturn(TestData.WALLET_WITH_ID_AND_TRANSACTION);
		Integer walletId = 1;
		
		// Execute
		ResponseEntity<Wallet> response = walletApi.registerTransaction(TestData.TRANSACTION_ID, walletId, TestData.WALLET_TRANSACTION_REQUEST_DEBIT);

		// Verify
		Assert.assertEquals(TestData.WALLET_WITH_ID_AND_TRANSACTION, response.getBody());
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		verify(walletServiceMock).registerTransaction(TestData.TRANSACTION_ID, walletId, TestData.WALLET_TRANSACTION_REQUEST_DEBIT);
	}
}
