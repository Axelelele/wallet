package se.axel.wallet.api.exception.mapper;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import se.axel.wallet.api.common.transactionid.exception.DuplicateTransactionIdException;
import se.axel.wallet.api.common.transactionid.exception.InvalidTransactionIdException;
import se.axel.wallet.api.common.transactionid.exception.MissingTransactionIdException;
import se.axel.wallet.exception.WalletInsufficientFundsException;
import se.axel.wallet.exception.WalletNotFoundException;
import se.axel.wallet.transaction.exception.WalletTransactionInvalidAmountException;

@RunWith(Parameterized.class)
public class ResponseExceptionMapperTest {


	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { 
			{ new DuplicateTransactionIdException(null), HttpStatus.FORBIDDEN, MediaType.TEXT_PLAIN } ,
			{ new WalletInsufficientFundsException(null), HttpStatus.CONFLICT, MediaType.TEXT_PLAIN } ,
			{ new InvalidTransactionIdException(null), HttpStatus.BAD_REQUEST, MediaType.TEXT_PLAIN } ,
			{ new MissingTransactionIdException(null), HttpStatus.BAD_REQUEST, MediaType.TEXT_PLAIN } ,
			{ new WalletTransactionInvalidAmountException(null), HttpStatus.BAD_REQUEST, MediaType.TEXT_PLAIN } ,
			{ new WalletNotFoundException(null), HttpStatus.NOT_FOUND, MediaType.TEXT_PLAIN }
		});
	}

	private ResponseExceptionMapper responseMapper = new ResponseExceptionMapper();
	
	private Throwable exceptionToMap;
	private HttpStatus expectedHttpStatus;
	private MediaType expectedMediaType;
	
	public ResponseExceptionMapperTest(Throwable exceptionToMap, HttpStatus expectedHttpStatus, MediaType expectedMediaType) {
		this.exceptionToMap = exceptionToMap;
		this.expectedHttpStatus = expectedHttpStatus;
		this.expectedMediaType = expectedMediaType;
	}
	
	@Test
	public void test_mapException() {
		ResponseEntity<String> response = responseMapper.mapException(exceptionToMap, null);
		Assert.assertEquals(expectedHttpStatus, response.getStatusCode());
		Assert.assertEquals(expectedMediaType.toString(), response.getHeaders().get(HttpHeaders.CONTENT_TYPE).get(0));
	}

}
