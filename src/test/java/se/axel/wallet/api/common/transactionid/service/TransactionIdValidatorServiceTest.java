package se.axel.wallet.api.common.transactionid.service;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import se.axel.wallet.api.common.transactionid.exception.DuplicateTransactionIdException;
import se.axel.wallet.api.common.transactionid.exception.InvalidTransactionIdException;
import se.axel.wallet.api.common.transactionid.exception.MissingTransactionIdException;
import se.axel.wallet.test.TestData;
import se.axel.wallet.transaction.domain.WalletTransaction;
import se.axel.wallet.transaction.repository.WalletTransactionRepository;

@RunWith(value = MockitoJUnitRunner.class)
public class TransactionIdValidatorServiceTest {

	@Mock
	private WalletTransactionRepository transactionRepository;
	
	private TransactionIdValidatorService transactionIdValidatorService;
	
	@Before
	public void setup() {
		transactionIdValidatorService = new TransactionIdValidatorService(transactionRepository);
	}
	
	@Test
	public void test_validateTransactionId() {
		//Execute
		boolean validationResult = transactionIdValidatorService.validateTransactionId(TestData.TRANSACTION_ID);
		
		//Verify
		Assert.assertTrue(validationResult);
	}
	
	@Test(expected = MissingTransactionIdException.class)
	public void test_validateTransactionId_MissingTransactionId() {
		//Setup
		String transactionId = null;
		
		//Execute
		transactionIdValidatorService.validateTransactionId(transactionId);
		
		//Verify
		fail("Missing transaction id should throw exception of type MissingTransactionIdException.");
	}
	
	@Test(expected = InvalidTransactionIdException.class)
	public void test_validateTransactionId_InvalidTransactionId() {
		//Setup
		String transactionId = "abc123";
		
		//Execute
		transactionIdValidatorService.validateTransactionId(transactionId);
		
		//Verify
		fail("Invalid transaction id should throw exception of type InvalidTransactionIdException.");
	}
	
	@Test(expected = DuplicateTransactionIdException.class)
	public void test_validateTransactionId_DuplicateTransactionId() {
		//Setup
		WalletTransaction transaction = TestData.WALLET_TRANSACTION_WITH_TRANSACTION_ID_DEBIT;
		when(transactionRepository.getById(anyString())).thenReturn(transaction);
		
		//Execute
		transactionIdValidatorService.validateTransactionId(transaction.getTransactionId());
		
		//Verify
		fail("Duplicate transaction id should throw exception of type DuplicateTransactionIdException.");
	}
}
