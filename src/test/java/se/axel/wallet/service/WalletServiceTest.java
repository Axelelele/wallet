package se.axel.wallet.service;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.LockModeType;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import se.axel.wallet.domain.Wallet;
import se.axel.wallet.exception.WalletInsufficientFundsException;
import se.axel.wallet.exception.WalletNotFoundException;
import se.axel.wallet.repository.WalletRepository;
import se.axel.wallet.test.TestData;
import se.axel.wallet.transaction.domain.WalletTransaction;
import se.axel.wallet.transaction.domain.WalletTransactionRequest;
import se.axel.wallet.transaction.exception.WalletTransactionInvalidAmountException;

@RunWith(MockitoJUnitRunner.class)
public class WalletServiceTest {

	private WalletService walletService;
	
	@Mock
	private WalletRepository walletRepository;
	
	@Before
	public void setup() {
		walletService = new WalletService(walletRepository);
	}
	
	@Test
	public void test_createWallet() {
		//Setup
		when(walletRepository.insert(any(Wallet.class))).thenReturn(TestData.NEW_WALLET_WITH_ID);
		
		//Execute
		walletService.createWallet();
		
		//Verify
		ArgumentCaptor<Wallet> walletArgument = ArgumentCaptor.forClass(Wallet.class);
		
		verify(walletRepository).insert(walletArgument.capture());
		Assert.assertNull(walletArgument.getValue().getWalletId());
		Assert.assertEquals(0, walletArgument.getValue().getBalance());
	}
	
	@Test
	public void test_getWalletById() {
		//Setup
		when(walletRepository.getById(any(Integer.class), any(LockModeType.class))).thenReturn(TestData.NEW_WALLET_WITH_ID);
		Integer walletId = 1;
		
		//Execute
		walletService.getWalletById(walletId);
		
		//Verify
		ArgumentCaptor<Integer> walletIdArgument = ArgumentCaptor.forClass(Integer.class);
		ArgumentCaptor<LockModeType> lockModeArgument = ArgumentCaptor.forClass(LockModeType.class);
		
		verify(walletRepository).getById(walletIdArgument.capture(), lockModeArgument.capture());
		Assert.assertEquals(walletId, walletIdArgument.getValue());
		Assert.assertEquals(LockModeType.NONE, lockModeArgument.getValue());
	}
	
	@Test(expected = WalletNotFoundException.class)
	public void test_getWalletById_WalletNotFound() {
		//Setup
		when(walletRepository.getById(any(Integer.class), any(LockModeType.class))).thenReturn(null);
		Integer walletId = 1;
		
		//Execute
		walletService.getWalletById(walletId);
		
		//Verify
		fail("Missing wallet should throw exception of type WalletNotFoundException.");
	}
	
	@Test
	public void test_getWalletByIdForUpdate() {
		//Setup
		when(walletRepository.getById(any(Integer.class), any(LockModeType.class))).thenReturn(TestData.NEW_WALLET_WITH_ID);
		Integer walletId = 1;
		
		//Execute
		walletService.getWalletByIdForUpdate(walletId);
		
		//Verify
		ArgumentCaptor<Integer> walletIdArgument = ArgumentCaptor.forClass(Integer.class);
		ArgumentCaptor<LockModeType> lockModeArgument = ArgumentCaptor.forClass(LockModeType.class);
		
		verify(walletRepository).getById(walletIdArgument.capture(), lockModeArgument.capture());
		Assert.assertEquals(walletId, walletIdArgument.getValue());
		Assert.assertEquals(LockModeType.PESSIMISTIC_FORCE_INCREMENT, lockModeArgument.getValue());
	}
	
	@Test(expected = WalletNotFoundException.class)
	public void test_getWalletByIdForUpdate_WalletNotFound() {
		//Setup
		when(walletRepository.getById(any(Integer.class), any(LockModeType.class))).thenReturn(null);
		Integer walletId = 1;
		
		//Execute
		walletService.getWalletByIdForUpdate(walletId);
		
		//Verify
		fail("Missing wallet should throw exception of type WalletNotFoundException.");
	}

	@Test
	public void test_registerTransaction_Credit() {
		//Setup
		WalletTransactionRequest transactionRequest = TestData.WALLET_TRANSACTION_REQUEST_CREDIT;
		Wallet initialWallet = TestData.NEW_WALLET_WITH_ID;
		when(walletRepository.getById(any(Integer.class), any(LockModeType.class))).thenReturn(initialWallet);
		long initialBalance = initialWallet.getBalance();
		long expectedBalance = initialBalance + transactionRequest.getAmount();

		//Execute
		walletService.registerTransaction(TestData.TRANSACTION_ID, initialWallet.getWalletId(), transactionRequest);
	
		//Verify
		ArgumentCaptor<Wallet> updatedWalletArgument = ArgumentCaptor.forClass(Wallet.class);
		verify(walletRepository).update(updatedWalletArgument.capture());
		Wallet updatedWallet = updatedWalletArgument.getValue();
		
		Assert.assertEquals(1, updatedWallet.getTransactions().size());
		Assert.assertEquals(expectedBalance, updatedWallet.getBalance());
		
		WalletTransaction transaction = updatedWallet.getTransactions().get(0);
		Assert.assertEquals(TestData.TRANSACTION_ID, transaction.getTransactionId());
		Assert.assertTrue(updatedWallet.getLastTransactionTime().equals(transaction.getTransactionTime()));

	}
	
	@Test
	public void test_registerTransaction_Debit() {
		//Setup
		WalletTransactionRequest transactionRequest = TestData.WALLET_TRANSACTION_REQUEST_DEBIT;
		Wallet initialWallet = new Wallet.Builder().balance(500).walletId(1).build();
		when(walletRepository.getById(any(Integer.class), any(LockModeType.class))).thenReturn(initialWallet);
		long initialBalance = initialWallet.getBalance();
		long expectedBalance = initialBalance - transactionRequest.getAmount();

		//Execute
		walletService.registerTransaction(TestData.TRANSACTION_ID, initialWallet.getWalletId(), transactionRequest);
	
		//Verify
		ArgumentCaptor<Wallet> updatedWalletArgument = ArgumentCaptor.forClass(Wallet.class);
		verify(walletRepository).update(updatedWalletArgument.capture());
		Wallet updatedWallet = updatedWalletArgument.getValue();
		
		Assert.assertEquals(1, updatedWallet.getTransactions().size());
		Assert.assertEquals(expectedBalance, updatedWallet.getBalance());
		
		WalletTransaction transaction = updatedWallet.getTransactions().get(0);
		Assert.assertEquals(TestData.TRANSACTION_ID, transaction.getTransactionId());
		Assert.assertTrue(updatedWallet.getLastTransactionTime().equals(transaction.getTransactionTime()));

	}
	
	@Test(expected = WalletInsufficientFundsException.class)
	public void test_registerTransaction_Debit_InsufficientFunds() {
		//Setup
		WalletTransactionRequest transactionRequest = TestData.WALLET_TRANSACTION_REQUEST_DEBIT;
		long insufficientWalletBalance = transactionRequest.getAmount() / 2;
		Wallet initialWallet = new Wallet.Builder().balance(insufficientWalletBalance).walletId(1).build();
		when(walletRepository.getById(any(Integer.class), any(LockModeType.class))).thenReturn(initialWallet);

		//Execute
		walletService.registerTransaction(TestData.TRANSACTION_ID, initialWallet.getWalletId(), transactionRequest);
		
		//Verify
		fail("Insufficient funds should throw exception of type WalletInsufficientFundsException.");
	}
	
	@Test(expected = WalletTransactionInvalidAmountException.class)
	public void test_registerTransaction_InvalidTransactionAmount() {
		//Setup
		WalletTransactionRequest invalidTransactionRequest = TestData.WALLET_TRANSACTION_REQUEST_INVALID_AMOUNT;
		Wallet walletallet = TestData.NEW_WALLET_WITH_ID;
		when(walletRepository.getById(any(Integer.class), any(LockModeType.class))).thenReturn(walletallet);

		//Execute
		walletService.registerTransaction(TestData.TRANSACTION_ID, walletallet.getWalletId(), invalidTransactionRequest);
		
		//Verify
		fail("Invalid transaction amount should throw exception of type WalletTransactionInvalidAmountException.");
	}
	
}
