package se.axel.wallet.test;

import java.util.Arrays;

import se.axel.wallet.domain.Wallet;
import se.axel.wallet.transaction.domain.WalletTransaction;
import se.axel.wallet.transaction.domain.WalletTransactionRequest;
import se.axel.wallet.transaction.domain.WalletTransactionType;

public class TestData {
	public static String TRANSACTION_ID = "a6c81fa7-663-42b1-b257-87023b8d5543";

	public static WalletTransactionRequest WALLET_TRANSACTION_REQUEST_DEBIT = new WalletTransactionRequest.Builder()
			.amount(100)
			.transactionType(WalletTransactionType.DEBIT)
			.build();

	public static WalletTransactionRequest WALLET_TRANSACTION_REQUEST_CREDIT = new WalletTransactionRequest.Builder()
			.amount(100)
			.transactionType(WalletTransactionType.CREDIT)
			.build();

	public static WalletTransactionRequest WALLET_TRANSACTION_REQUEST_INVALID_AMOUNT = new WalletTransactionRequest.Builder()
			.amount(-1000)
			.transactionType(WalletTransactionType.DEBIT)
			.build();
	
	public static WalletTransaction WALLET_TRANSACTION_WITH_TRANSACTION_ID_DEBIT = new WalletTransaction.Builder()
			.amount(100)
			.transactionType(WalletTransactionType.DEBIT)
			.transactionId(TRANSACTION_ID)
			.build();

	public static WalletTransaction WALLET_TRANSACTION_CREDIT = new WalletTransaction.Builder()
			.amount(100)
			.transactionType(WalletTransactionType.CREDIT)
			.build();

	public static WalletTransaction WALLET_TRANSACTION_DEBIT = new WalletTransaction.Builder()
			.amount(100)
			.transactionType(WalletTransactionType.DEBIT)
			.build();

	public static Wallet NEW_WALLET = new Wallet.Builder().balance(0).build();
	public static Wallet NEW_WALLET_WITH_ID = new Wallet.Builder().balance(0).build();
	public static Wallet WALLET_WITH_ID_AND_TRANSACTION = new Wallet.Builder().balance(0)
			.walletId(1).transactions(Arrays.asList(WALLET_TRANSACTION_WITH_TRANSACTION_ID_DEBIT))
			.build();

}
