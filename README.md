# Wallet Service #

This document describes the wallet service and its API.

# API #

The service hosts three endpoints:

`POST /wallet`:
Creates and returns a new wallet.

`POST /wallet/{walletId}`:
Registers a transaction to the specified walletId.
	
`GET /wallet/{walletId}`:
Returns a wallet and any previous transactions belonging to the specified walletId.

Full schema definitions can be found in the wallet.yaml file.

# Starting the service #

The application is built on the Spring Boot framework. Spring Boot provides an embedded Tomcat server which means that it's ready to "plug and play".
To run the service locally, you're going to need Maven and Java (min. 8). Navigate to the root folder of the project, and execute "mvn spring-boot:run" in your command prompt.
By default, the service should be listening to port 8080.

# Examples #

The typical flow for a user would look like this:

### 1. Create a wallet ###
Send an empty POST request to `localhost:8080/wallet/`.
You'll get a response that looks like this:
```
{
    "walletId": 1,
    "balance": 0
}
```
### 2. Register transactions to your wallet ###
Send a POST request to `localhost:8080/wallet/1`.
For this request, you're going to need to provide an HTTP header "Transaction-ID" in the form of a valid and unique UUID.
The request body for this request is defined in the wallet.yaml file, but here is an example:
```
{
    "transactionType":"CREDIT",
    "amount":1000
}
```
The response will look like this:
```
{
    "walletId": 1,
    "balance": 1000,
    "transactions": [
        {
            "transactionId": "a2c21fa4-66a3-42b1-b257-87023b8d5543",
            "transactionType": "CREDIT",
            "transactionTime": 1525090271213,
            "amount": 1000
        }
    ],
    "lastTransactionTime": 1525090271213
}
```
### 3. Retrieve your wallet ###
Send a GET request to `localhost:8080/wallet/1`.
The response for this request is the same as when you register a transaction.

# Sidenote #
For the scope of the assignment, I chose to build the application with an embedded Derby database.
Since the database is in-memory, it is highly unsuitable to be used in a production environment.
All wallets are contained within separate instances of the service, and a restart wipes all data.
An external database would need to be configured for the data to be properly persisted.
The application also lacks any security, other than the constraints of the assignment.